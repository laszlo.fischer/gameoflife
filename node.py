class Node:
    def __init__(self, pos_x, pos_y, alive):
        self.alive_now = alive
        self.alive_next = False
        self.pos_x = pos_x
        self.pos_y = pos_y
        name = str(pos_x) + "," + str(pos_y)
        self.name = str(name) + '\t\t'

    def set_next_state(self, number_of_live_neighbours):
        if number_of_live_neighbours == 3:
            self.alive_next = True
        elif 2 > number_of_live_neighbours or number_of_live_neighbours > 3:
            self.alive_next = False
        else:
            self.alive_next = self.alive_now

    def get_state_now(self):
        return self.alive_now

    def print_state_now(self):
        state = ":)\t"
        if not self.alive_now:
            state = " " + '\t'
        print(state, end="")

    def print_name(self):
        print(self.name, end="")

    def next_gen(self):
        self.alive_now = self.alive_next