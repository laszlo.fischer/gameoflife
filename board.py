from random import random
import node
import time


class Board:
    size_x = 20
    size_y = 20
    nodes = []

    def __init__(self):
        for y in range(self.size_y):
            row = []
            for x in range(self.size_x):
                alive = False
                if random() < 0.5:
                    alive = True
                this_node = node.Node(x, y, alive)
                row.append(this_node)
            self.nodes.append(row)

    def print_board(self):
        for y in range(self.size_y):
            for x in range(self.size_x):
                # nodes[y][x].print_name()
                self.nodes[y][x].print_state_now()
            print("")

    def calculate_next_gen(self):
        e = 0
        for y in range(self.size_y):
            for x in range(self.size_x):
                number_alive = 0
                for i in range(-1, 2):
                    for j in range(-1, 2):
                        try:
                            if i == 0 and j == 0:
                                continue
                            elif x + j > -1 and y + i > -1:
                                if self.nodes[y + i][x + j].get_state_now():
                                    number_alive = number_alive + 1
                                    # print(str(x + j) + "," + str(y + i), end="\t")
                        except IndexError as error:
                            e = e + 1
                            # print("error: ", e)
                # print("Node:", str(x) + "," + str(y), "neighbours: ", number_alive)
                self.nodes[y][x].set_next_state(number_alive)

    def iterate_gen(self):
        for y in range(self.size_y):
            for x in range(self.size_x):
                self.nodes[y][x].next_gen()


board = Board()

max_gen = 100
i = 0
while i < max_gen:
    print("Generation: ", i+1)
    board.print_board()
    board.calculate_next_gen()
    board.iterate_gen()
    i += 1
    time.sleep(0.5)


# for i in range(0, board.size_y):
#     print("========", end="")
# print("")
